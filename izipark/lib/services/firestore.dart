import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class FirestoreService {
  // obting collection de vehicles
  final CollectionReference vehicles =
      FirebaseFirestore.instance.collection('vehicles');

  // obting collection de registre
  final CollectionReference registre =
      FirebaseFirestore.instance.collection('registre');

  // MOVE: moure un document de vehicles a registre
  Future<void> moveVehicleToRegistre(String matricula) async {
    var qs = await vehicles.where('matricula', isEqualTo: matricula).get();

    var vehicleDoc = qs.docs.first;

    Map<String, dynamic> vehicleData =
        vehicleDoc.data() as Map<String, dynamic>;

    WriteBatch batch = FirebaseFirestore.instance.batch();

    DocumentReference registreRef =
        FirebaseFirestore.instance.collection('registre').doc();
    batch.set(registreRef, vehicleData);

    batch.delete(vehicleDoc.reference);

    await batch.commit().then((event) {
      debugPrint('El vehicle ha estat mogut correctament');
    }).catchError((e) {
      debugPrint('Error al moure el vehicle: $e');
    });
  }

  // CREATE: afegir vehicle
  Future<void> addVehicle(String matricula, String marca, String model,
      String propietari, String telefon) {
    return vehicles.add({
      'matricula': matricula,
      'marca': marca,
      'model': model,
      'propietari': propietari,
      'telefon': telefon,
      'entrada': Timestamp.now().toDate().toString(),
      'sortida': 'Encara no ha sortit'
    });
  }

  // UPDATE: estableix l'hora de sortida
  Future<bool> updateVehicleSortida(String matricula) async {
    try {
      var qs = await vehicles.where('matricula', isEqualTo: matricula).get();
      var ds = qs.docs.first;

      Map<String, dynamic> updateData = {
        'sortida': Timestamp.now().toDate().toString(),
      };

      await ds.reference.update(updateData);

      return true;
    } on Exception catch (e) {
      debugPrint('Error al eliminar el vehicle, Error: $e');
      return false;
    }
  }

  // UPDATE: update del vehicle amb ID
  Future<void> updateVehicle(String docID, String matricula, String marca,
      String model, String propietari, int telefon) {
    return vehicles.doc(docID).update({
      'marca': marca,
      'timestamp': Timestamp.now(),
    });
  }

  // READ: get dels vehicles amb matricula
  Stream<QuerySnapshot> getVehiclesStreamByMatricula(String matricula,
      {String? page}) {
    if (page == 'search') {
      return vehicles.where('matricula', isEqualTo: matricula).snapshots();
    } else if (page == 'registre') {
      return registre.where('matricula', isEqualTo: matricula).snapshots();
    } else {
      return vehicles.where('matricula', isEqualTo: matricula).snapshots();
    }
  }

  // READ: comprova si aquest vehicle ja es troba a la BDD amb la matricula
  Future<bool> existsVehicleWithMatricula(String matricula) async {
    final querySnapshot =
        await vehicles.where('matricula', isEqualTo: matricula).get();
    return querySnapshot.docs.isNotEmpty;
  }

  // READ: get un vehicle concret amb ID
  Future<DocumentSnapshot> getVehicleStream(String docID) {
    final vehicleStream = vehicles.doc(docID).get();

    return vehicleStream;
  }

  // READ: get tots els vehicles de la BDD
  Stream<QuerySnapshot> getVehiclesStream({String? page}) {
    final Stream<QuerySnapshot<Object?>> vehiclesStream;

    if (page == 'search') {
      vehiclesStream =
          vehicles.orderBy('entrada', descending: true).snapshots();
    } else if (page == 'registre') {
      vehiclesStream =
          registre.orderBy('entrada', descending: true).snapshots();
    } else {
      vehiclesStream =
          vehicles.orderBy('entrada', descending: true).snapshots();
    }

    return vehiclesStream;
  }

  // DELETE: delete vehicle amb id
  Future<void> deleteVehicle(String docID) {
    return vehicles.doc(docID).delete();
  }

  // DELETE: delete vehicle amb matricula
  Future<bool> deleteVehicleMatricula(String matricula, {String? page}) async {
    if (page == 'search') {
      try {
        var qs = await vehicles.where('matricula', isEqualTo: matricula).get();

        for (var doc in qs.docs) {
          await doc.reference.delete();
        }

        return true;
      } on Exception catch (e) {
        debugPrint('Error al eliminar el vehicle, Error: $e');
        return false;
      }
    } else {
      try {
        var qs = await registre.where('matricula', isEqualTo: matricula).get();

        for (var doc in qs.docs) {
          await doc.reference.delete();
        }

        return true;
      } on Exception catch (e) {
        debugPrint('Error al eliminar el vehicle, Error: $e');
        return false;
      }
    }
  }
}
