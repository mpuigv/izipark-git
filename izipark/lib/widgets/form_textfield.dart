import 'package:flutter/material.dart';

class FormTextField extends StatelessWidget {
  final TextEditingController controller;
  final IconData icon;
  final Function()? onTap;
  final String hintText;
  final String labelText;
  final TextInputType keyboardType;

  const FormTextField({
    super.key,
    required this.controller,
    required this.hintText,
    required this.labelText,
    required this.keyboardType,
    required this.icon,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 0, left: 15, bottom: 0, right: 20),
      margin: const EdgeInsets.only(bottom: 5, left: 20, right: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black.withOpacity(0.05),
              offset: const Offset(0, 5),
              blurRadius: 5)
        ],
      ),
      child: TextFormField(
        decoration: InputDecoration(
          focusedBorder: InputBorder.none,
          border: InputBorder.none,
          hintText: hintText,
          prefixIcon: Icon(icon),
          labelText: labelText,
        ),
        controller: controller,
        keyboardType: keyboardType,
        onTap: onTap,
      ),
    );
  }
}
