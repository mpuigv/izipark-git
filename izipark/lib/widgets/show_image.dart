import 'package:flutter/material.dart';

class ShowImage extends StatelessWidget {
  final String? model;
  const ShowImage({super.key, this.model});
  
  @override
  Widget build(BuildContext context) {
    
  List<String> models = ['serie3','a3','stinger', 'supra'];
  String? modelAMostrar;
   
   if (models.contains(model)) {
     modelAMostrar = model;
   } else {
     modelAMostrar = 'serie3';
   } 
   
   print(model);
   print(modelAMostrar); 
    
    return Container(
      height: 250,
      margin: const EdgeInsets.all(8),
      padding: const EdgeInsets.all(8),
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8.0)),
        color: Color.fromRGBO(39, 142, 234, 1),
      ),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(8.0)),
        child: Image(
          image: AssetImage('assets/images/$modelAMostrar.jpg'),
        ),
      ),
    );
  }
}
