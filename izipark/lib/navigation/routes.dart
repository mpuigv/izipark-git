import 'package:go_router/go_router.dart';
import 'package:izipark/pages/history_page.dart';
import 'package:izipark/pages/home_page.dart';
import 'package:izipark/pages/inscribe_page.dart';
import 'package:izipark/pages/search_page.dart';
import 'package:izipark/pages/vehicle_info_history_page.dart';
import 'package:izipark/pages/vehicle_info_search_page.dart';
import 'package:izipark/pages/welcome_page.dart';

final GoRouter router = GoRouter(
  initialLocation: '/pages/welcome_page',
  routes: [
    GoRoute(
      path: '/pages/welcome_page',
      name: 'welcome_page',
      builder: (context, builder) {
        return const WelcomePage();
      },
    ),
    GoRoute(
      path: '/pages/home_page',
      name: 'home_page',
      builder: (context, builder) {
        return const HomePage();
      },
    ),
    GoRoute(
      path: '/pages/search_page',
      name: 'search_page',
      builder: (context, builder) {
        return const SearchPage();
      },
    ),
    GoRoute(
      path: '/pages/vehicle_info_search_page/:matricula',
      name: 'vehicle_info_search_page',
      builder: (context, state) {
        final matricula = state.extra! as String;

        return VehicleInfoSearchPage(matricula: matricula);
      },
    ),
        GoRoute(
      path: '/pages/vehicle_info_history_page/:matricula',
      name: 'vehicle_info_history_page',
      builder: (context, state) {
        final matricula = state.extra! as String;

        return VehicleInfoHistoryPage(matricula: matricula);
      },
    ),
    GoRoute(
      path: '/pages/history_page',
      name: 'history_page',
      builder: (context, builder) {
        return const HistoryPage();
      },
    ),
    GoRoute(
      path: '/pages/inscribe_page',
      name: 'inscribe_page',
      builder: (context, builder) {
        return const InscribePage();
      },
    ),
  ],
);
