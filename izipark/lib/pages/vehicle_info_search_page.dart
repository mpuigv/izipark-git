import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:izipark/navigation/routes.dart';
import 'package:izipark/services/firestore.dart';
import 'package:izipark/widgets/background.dart';
import 'package:izipark/widgets/show_image.dart';

class VehicleInfoSearchPage extends StatefulWidget {
  final String matricula;
  const VehicleInfoSearchPage({super.key, required this.matricula});

  @override
  State<VehicleInfoSearchPage> createState() => _VehicleInfoSearch();
}

class _VehicleInfoSearch extends State<VehicleInfoSearchPage> {
  // firestore
  final FirestoreService firestoreService = FirestoreService();

  // stream
  Stream<QuerySnapshot>? currentStream;

  String? model;

  @override
  void initState() {
    super.initState();
    currentStream =
        firestoreService.getVehiclesStreamByMatricula(widget.matricula);
  }

  @override
  Widget build(BuildContext context) {
    // firestore
    final FirestoreService firestoreService = FirestoreService();

    return Stack(children: [
      const BackgroundDecoration(),
      Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
              title: const Text('Informació vehicle',
                  style: TextStyle(color: Colors.white)),
              shadowColor: Colors.black,
              elevation: 5,
              backgroundColor: Colors.blue,
              leading: IconButton(
                onPressed: () {
                  router.pop();
                },
                icon: const Icon(Icons.arrow_back),
                color: Colors.white,
              )),
          body: Column(
            children: [
              Container(
                width: double.infinity,
                margin: const EdgeInsets.all(8.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: const Offset(0, 3),
                    ),
                  ],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: StreamBuilder<QuerySnapshot>(
                    stream: currentStream,
                    builder: (context, snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Center(child: CircularProgressIndicator());
                      }

                      if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                        return const Center(
                            child: Text("No hi ha dades d'aquest vehicle."));
                      }

                      DocumentSnapshot document = snapshot.data!.docs.first;
                      Map<String, dynamic> data =
                          document.data() as Map<String, dynamic>;

                      // SchedulerBinding.instance.addPostFrameCallback((_) {
                      //   setState(() {
                      //     model = data['model'].toString().toLowerCase();
                      //   });
                      // });

                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text("Matrícula: ${data['matricula']}",
                              style: const TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold)),
                          const SizedBox(height: 10),
                          Text("Marca: ${data['marca']}",
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(height: 10),
                          Text("Model: ${data['model']}",
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(height: 10),
                          Text("Propietari: ${data['propietari']}",
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(height: 10),
                          Text("Telèfon: ${data['telefon']}",
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(height: 10),
                          Text("Data entrada: ${data['entrada']}",
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(height: 10),
                          Text("Data sortida: ${data['sortida']}",
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(height: 20),
                          ShowImage(
                            model: data['model'].toString().toLowerCase(),
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () async {
                      // Sortida vehicle
                      bool updateReturn = await firestoreService
                          .updateVehicleSortida(widget.matricula);
                      if (updateReturn) {
                        firestoreService
                            .moveVehicleToRegistre(widget.matricula);
                        router.goNamed('home_page');
                      }
                    },
                    style: TextButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: const Color.fromARGB(255, 0, 136, 200),
                    ),
                    child: const Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.exit_to_app),
                        SizedBox(width: 10),
                        Text('Establir sortida')
                      ],
                    ),
                  ),
                  const SizedBox(width: 10),
                  ElevatedButton(
                    onPressed: () async {
                      bool deleteReturn = await firestoreService
                          .deleteVehicleMatricula(widget.matricula);
                      // si el vehicle es eliminat mostra una alerta
                      if (deleteReturn) {
                        if (!mounted) return;
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    router.goNamed('home_page');
                                  },
                                  child: const Text('ok'))
                            ],
                            title: const Text('Notificació'),
                            contentPadding: const EdgeInsets.all(20),
                            content:
                                const Text('El vehicle ha estat eliminat.'),
                          ),
                        );
                      }
                      // si no ha estat eliminat mostrarà una alerta corresponent
                      else {
                        if (!mounted) return;
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    router.goNamed('home_page');
                                  },
                                  child: const Text('ok'))
                            ],
                            title: const Text('Notificació'),
                            contentPadding: const EdgeInsets.all(20),
                            content:
                                const Text('El vehicle NO ha estat eliminat.'),
                          ),
                        );
                      }
                    },
                    style: TextButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: Colors.red,
                    ),
                    child: const Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Icon(Icons.exit_to_app),
                        SizedBox(width: 10),
                        Text('Eliminar vehicle')
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),
              // ShowImage(
              //   model: model,
              // ),
            ],
          ))
    ]);
  }
}
