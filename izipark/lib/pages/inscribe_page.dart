import 'package:flutter/material.dart';
import 'package:izipark/navigation/routes.dart';
import 'package:izipark/services/firestore.dart';
import 'package:izipark/widgets/background.dart';
import 'package:izipark/widgets/form_textfield.dart';

class InscribePage extends StatefulWidget {
  const InscribePage({super.key});

  @override
  State<InscribePage> createState() => _InscribePageState();
}

class _InscribePageState extends State<InscribePage> {
  // firestore
  final FirestoreService firestoreService = FirestoreService();

  // controllers
  final matriculaController = TextEditingController();
  final marcaController = TextEditingController();
  final modelController = TextEditingController();
  final propietariController = TextEditingController();
  final telefonController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const BackgroundDecoration(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: const Text('Entrada de vehicles',
                style: TextStyle(color: Colors.white)),
            shadowColor: Colors.black,
            elevation: 5,
            backgroundColor: Colors.blue,
            leading: IconButton(
              onPressed: () {
                router.pop();
              },
              icon: const Icon(Icons.arrow_back),
              color: Colors.white,
            ),
          ),
          body: Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const SizedBox(height: 30),
                  FormTextField(
                    controller: matriculaController,
                    hintText: '1231ABC',
                    labelText: 'Matricula',
                    keyboardType: TextInputType.text,
                    icon: Icons.nineteen_mp_rounded,
                  ),
                  FormTextField(
                    controller: marcaController,
                    hintText: 'BMW',
                    labelText: 'Marca',
                    keyboardType: TextInputType.text,
                    icon: Icons.car_repair,
                  ),
                  FormTextField(
                    controller: modelController,
                    hintText: 'Serie3',
                    labelText: 'Model',
                    keyboardType: TextInputType.text,
                    icon: Icons.car_repair,
                  ),
                  FormTextField(
                    controller: propietariController,
                    hintText: 'Miquel',
                    labelText: 'Nom propietari',
                    keyboardType: TextInputType.text,
                    icon: Icons.person,
                  ),
                  FormTextField(
                    controller: telefonController,
                    hintText: '678964632',
                    labelText: 'Telèfon',
                    keyboardType: TextInputType.text,
                    icon: Icons.phone,
                  ),
                  const SizedBox(height: 30),
                  ElevatedButton(
                      onPressed: () async {
                        // registrar a la base de dades
                        bool existeix =
                            await firestoreService.existsVehicleWithMatricula(
                                matriculaController.text);
                        if (existeix) {
                          if (!mounted) return;
                          showDialog(
                              context: context,
                              builder: (context) => AlertDialog(
                                    actions: [
                                      TextButton(
                                          onPressed: () {
                                            router.goNamed('home_page');
                                          },
                                          child: const Text('ok'))
                                    ],
                                    title: const Text('Notificació'),
                                    contentPadding: const EdgeInsets.all(20),
                                    content: const Text('Vehicle ja existeix.'),
                                  ));
                          return;
                        }

                        firestoreService.addVehicle(
                          matriculaController.text.toUpperCase(),
                          marcaController.text.toUpperCase(),
                          modelController.text.toUpperCase(),
                          propietariController.text.toUpperCase(),
                          telefonController.text,
                        );

                        if (!mounted) return;
                        showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                                  actions: [
                                    TextButton(
                                        onPressed: () {
                                          router.goNamed('home_page');
                                        },
                                        child: const Text('ok'))
                                  ],
                                  title: const Text('Notificació'),
                                  contentPadding: const EdgeInsets.all(20),
                                  content: const Text(
                                      'Vehicle registrat correctament.'),
                                ));
                      },
                      style: TextButton.styleFrom(
                        foregroundColor: Colors.white,
                        backgroundColor: const Color.fromARGB(255, 0, 136, 200),
                      ),
                      child: const Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.person_add),
                          SizedBox(width: 10),
                          Text('Registrar')
                        ],
                      )),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
