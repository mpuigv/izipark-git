import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:izipark/services/firestore.dart';
import 'package:izipark/widgets/form_textfield.dart';

import '../navigation/routes.dart';
import '../widgets/background.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  // firestore
  final FirestoreService firestoreService = FirestoreService();

  // text controller
  final TextEditingController textController = TextEditingController();

  // stream
  Stream<QuerySnapshot>? currentStream;

  @override
  void initState() {
    super.initState();
    currentStream = firestoreService.getVehiclesStream(page: 'search');
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      const BackgroundDecoration(),
      Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: const Text('Buscador de vehicles',
                style: TextStyle(color: Colors.white)),
            shadowColor: Colors.black,
            elevation: 5,
            backgroundColor: Colors.blue,
            leading: IconButton(
              onPressed: () {
                router.pop();
              },
              icon: const Icon(Icons.arrow_back),
              color: Colors.white,
            ),
          ),
          body: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                const SizedBox(height: 30),
                FormTextField(
                    controller: textController,
                    hintText: '1023CJU',
                    labelText: 'Matricula',
                    keyboardType: TextInputType.text,
                    icon: Icons.nineteen_mp_rounded),
                const SizedBox(height: 10),
                ElevatedButton(
                    onPressed: () {
                      // buscar a la base de dades
                      setState(
                        () {
                          // actualitza el stream
                          currentStream = firestoreService
                              .getVehiclesStreamByMatricula(textController.text,
                                  page: 'search');

                          if (textController.text == '') {
                            currentStream =
                                firestoreService.getVehiclesStream(page: 'search');
                          }
                        },
                      );
                    },
                    style: TextButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: const Color.fromARGB(255, 0, 136, 200),
                    ),
                    child: const Row(mainAxisSize: MainAxisSize.min, children: [
                      Icon(Icons.manage_search),
                      SizedBox(width: 10),
                      Text('Cercar')
                    ])),
                const SizedBox(
                  height: 20,
                ),
                StreamBuilder<QuerySnapshot>(
                    stream: currentStream,
                    builder: (context, snapshot) {
                      // certeza de que hi ha data
                      if (snapshot.hasData) {
                        List vehiclesList = snapshot.data!.docs;
                        debugPrint(snapshot.data.toString());

                        if (vehiclesList.isNotEmpty) {
                          return Expanded(
                              child: ListView.builder(
                                  itemCount: vehiclesList.length,
                                  itemBuilder: (context, index) {
                                    // obting tots els docs
                                    DocumentSnapshot document =
                                        vehiclesList[index];

                                    // obting el vehicle del doc
                                    Map<String, dynamic> data =
                                        document.data() as Map<String, dynamic>;
                                    return Container(
                                        margin: const EdgeInsets.only(
                                            bottom: 5, left: 20, right: 20),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                          boxShadow: [
                                            BoxShadow(
                                              color:
                                                  Colors.grey.withOpacity(0.5),
                                              spreadRadius: 1,
                                              blurRadius: 5,
                                              offset: const Offset(0, 3),
                                            ),
                                          ],
                                        ),
                                        child: ListTile(
                                          title: Text(data['matricula']),
                                          subtitle: Text(
                                              "Propietari: ${data['propietari']}, Marca: ${data['marca']}"),
                                          onTap: () {
                                            router.push(
                                                '/pages/vehicle_info_search_page/:matricula',
                                                extra: data['matricula']);
                                          },
                                        ));
                                  }));
                        } else {
                          return const Padding(
                            padding: EdgeInsets.all(8.0),
                            child: Text('No hi ha vehicles...'),
                          );
                        }
                      } else {
                        return const Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Text('No hi ha dades...'),
                        );
                      }
                    })
              ])))
    ]);
  }
}
