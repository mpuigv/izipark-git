import 'package:flutter/material.dart';
import 'package:izipark/navigation/routes.dart';
import 'package:izipark/widgets/background.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});

  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const BackgroundDecoration(),
        Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              title: const Text('Mapa del Parking',
                  style: TextStyle(color: Colors.white)),
              shadowColor: Colors.black,
              elevation: 5,
              backgroundColor: Colors.blue,
              leading: IconButton(
                onPressed: () {
                  router.pop();
                },
                icon: const Icon(Icons.arrow_back),
                color: Colors.white,
              ),
            ),
            body: const Center())
      ],
    );
  }
}
