import 'package:flutter/material.dart';
import 'package:izipark/navigation/routes.dart';
import 'package:izipark/widgets/background.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const BackgroundDecoration(),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: const Center(
                child: Text('IziPark', style: TextStyle(color: Colors.white))),
            shadowColor: Colors.black,
            elevation: 5,
            backgroundColor: Colors.blue,
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    onPressed: () {
                      router.pushNamed('search_page');
                    },
                    style: TextButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: const Color.fromARGB(255, 0, 136, 200),
                    ),
                    child: const Text('Cercar vehicle')),
                const SizedBox(height: 30),
                ElevatedButton(
                    onPressed: () {
                      router.pushNamed('inscribe_page');
                    },
                    style: TextButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: const Color.fromARGB(255, 0, 136, 200),
                    ),
                    child: const Text('Registrar vehicle')),
                const SizedBox(height: 30),
                ElevatedButton(
                    onPressed: () {
                      router.pushNamed('history_page');
                    },
                    style: TextButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: const Color.fromARGB(255, 0, 136, 200),
                    ),
                    child: const Text('Registre sortides')),
                const SizedBox(height: 30),
                ElevatedButton(
                    onPressed: () {
                      // anar a la pàgina Configuracio
                    },
                    style: TextButton.styleFrom(
                      foregroundColor: Colors.white,
                      backgroundColor: const Color.fromARGB(201, 222, 82, 82),
                    ),
                    child: const Text('Configuració')),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
